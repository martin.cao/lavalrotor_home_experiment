"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    vec_accel = []
    for ii in range(len(x)):
        vec_accel.append(np.sqrt(x[ii]**2+y[ii]**2+z[ii]**2))
        
    return(np.array(vec_accel))


def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    new_time = np.linspace(np.min(time),np.max(time),len(time))
    new_data = np.interp(new_time,time,data)
    
    return([np.array(new_time), np.array(new_data)])


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    # Mittelwert auf Null verschieben
    x = x-np.mean(x)
    #print(np.mean(data))

    X = np.fft.fft(x)
    N = len(X)
    sr = N/(np.max(time)-np.min(time))
    n = np.arange(N)
    T = N/sr
    freq = n/T
    
    n_oneside = N//2
    f_oneside = freq[:n_oneside]
    return([X[:n_oneside],f_oneside])
